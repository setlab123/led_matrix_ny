
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void clock(void);
void lat(void);
void oe(void);


//typedef struct{
//	uint64_t red_bin_u[4][32];
//	uint64_t green_bin_u[4][32];
//	uint64_t blue_bin_u[4][32];
//
//} Serialcommand;
//
//
//volatile Serialcommand command;


uint64_t red_bin[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x800000008000000, 0x40000000400, 0x1c0000001c000000, 0x40000000400, 0x0, 0xe0000000e00, 0x0, 0x0, 0x200, 0x801040000000c10, 0x40000, 0x810000000000000, 0x400000004000000, 0x60000001800, 0x200000002000400, 0x0, 0x1000000010040100, 0x114a0000001619, 0x200000002000800, 0x818083000004003, 0x50300000504a8000, 0x2221053f00002400, 0x91fe01d09217ff8, 0xe711103ff00f0110, 0x221ffffc22053fff, 0xff01001ffffe0018, 0xffffffffffffffff, 0xffcff9fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
};



uint64_t green_bin[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x800000008000000, 0x40000000400, 0x1c0000001c000000, 0x40000000400, 0x800000008000000, 0xe0000000e00, 0x1c0000001c000000, 0x40000000410, 0x1c0000001c000200, 0x8010e0000000e10, 0x2e0000002e040200, 0x8110e0000000e10, 0x3f0000003f040700, 0x8139d0000000718, 0x7d0000007d0e0b00, 0x1c3b9f0000001f3d, 0x6f8000006f8f0f80, 0x182ff780000039bf, 0xff800000ff9f1f80, 0x367fffb000007fff, 0xfff00001fff5bfc0, 0x7fffffff0000ffff, 0xf7ffe01df7dff9f8, 0xe7fefffff00fffef, 0xfffffffffffeffff, 0xffffffffffffffff, 0xe3ffffffe3f3ffff, 0xffcff1fffffff3ff, 0xe3ffffffe3f3ffff, 0xfffff1fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
};



uint64_t blue_bin[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xf7fffffff7ffffff, 0xfffffbfffffffbff, 0xe3ffffffe3ffffff, 0xfffffbfffffffbff, 0xf7fffffff7ffffff, 0xfffff1fffffff1ff, 0xe3ffffffe3ffffff, 0xfffffbffffffffff, 0xe3ffffffe3fffdff, 0xf7fef1fffffff3ff, 0xd1ffffffd1fbfdff, 0xf7eef1fffffff1ff, 0xc4ffffffc4fbf8ff, 0xf7ec60ffffffe1e7, 0x80ffffff80f1f0ff, 0xe3c460ffffffe1c6, 0x807fffff8070f17f, 0xe7c1427fffffc040, 0x27fffff0260e87f, 0xc198007fffffc001, 0x103ffffe1040c03f, 0xa220053fffffa000, 0x11ffffe010079ff, 0xe730103fffff0100, 0x229ffffc22843fff, 0xff01001ffffe0018, 0xe3ffffffe3f3ffff, 0xffcff1fffffff3ff, 0xe3ffffffe3f3ffff, 0xfffff1fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff},
};







uint64_t red_bin_ny__[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};
uint64_t green_bin_ny__[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};
uint64_t blue_bin_ny__[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};

uint64_t red_bin_ny__out[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};
uint64_t green_bin_ny__out[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};
uint64_t blue_bin_ny__out[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1e00000000000000, 0x2000010000000, 0x3f00000000000000, 0x6800034000000, 0x7f00000000000000, 0x580002c000000, 0xfe00000000000000, 0x160000b0000000, 0xfe00000000000000, 0x1e0000f0000000, 0xfe00060000000000, 0x3f8001fc000000, 0xfe00090000000000, 0x7f0003f80000ff, 0xffe0f10000000000, 0x1fe000ff000078f, 0xffe1f20000000000, 0xfe0007f000780f, 0xffe7f00000000000, 0x7000038078001, 0xff07f00000000000, 0x3ff81ffff80001, 0xff0ffc0000000000, 0x47e7e23f000001, 0xff1ff80000000000, 0x5ffe02fff00001, 0xff3fe00000000000, 0x23fc011fe00021, 0xff7f800000000000, 0x403c0201e00049, 0xc7ff000000000000, 0x40380201c00099, 0xc7ff000000000000, 0x20180100c0008f, 0xffff000000000000, 0xc0000600087, 0xffff800000000000, 0x60000300043, 0xffff800000000000, 0x30000180020, 0x8008000000000000, 0x1f, 0xffffe00000000000, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, },
};


uint64_t red_bin_ny[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffefffff7fffff07, 0xffdfffffffffffff, 0xfffffffffffff877, 0xefde6fffffffffff, 0xffffffffffff87f7, 0xffde4fffffffffff, 0xfffffffffff87fff, 0xfffecfffffffffff, 0xffff87e1fc07ffff, 0xfff003ffffffffff, 0xffff781ffbfffffe, 0x38e007ffffffffff, 0xffff7ffffbfffffe, 0x38c01fffffffffff, 0xfffefffff7ffffff, 0xff807fffffffffff, 0xfffffffffffffff7, 0xff00ffffffffffff, 0xffffffffffffffe7, 0xfe00ffffffffffff, 0xfffffffffffffff0, 0xffffffffffff, 0xfffffffffffffff8, 0x7fffffffffff, 0xfffffffffffffffc, 0x7fffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};



uint64_t green_bin_ny[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xe1ffffffffffffff, 0xfffdffffefffffff, 0xc3ffffffffffffff, 0xfff97fffcbffffff, 0x83ffffffffffffff, 0xfffa7fffd3ffffff, 0x1ffffffffffffff, 0xfff9ffffcfffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xeffffeffffffffff, 0xffefffff7fffff04, 0xfe5fefffffffffff, 0xfefffff7fffff874, 0xee5ffdffffffffff, 0xffffffffffff87f4, 0xfe5fbfffffffffff, 0xfffffffffff87ffe, 0x7cffffffffffffff, 0xffff87e1fc07fffe, 0x38f003ffffffffff, 0xffff781ffbfffffe, 0x38e007ffffffffff, 0xffbf7ffdfbfffffe, 0xc01fffffffffff, 0xfffefffff7fffffe, 0x807fffffffffff, 0xfffffffffffffff6, 0x3800ffffffffffff, 0xffffffffffffffe7, 0xfe00ffffffffffff, 0xffdffffefffffff0, 0xffffffffffff, 0xfffffffffffffff8, 0x7fffffffffff, 0xfffffffffffffffc, 0x7fffffffffff, 0xfffffcffffe7ffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};



uint64_t blue_bin_ny[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xe1ffffffffffffff, 0xfffdffffefffffff, 0xc3ffffffffffffff, 0xfff97fffcbffffff, 0x83ffffffffffffff, 0xfffa7fffd3ffffff, 0x1ffffffffffffff, 0xffe9ffff4fffffff, 0xffffffffffffffff, 0xffe1ffff0fffffff, 0xfffffbffffffffff, 0xffcbfffe5fffffff, 0xfffffeffffffffff, 0xff88fffc47ffff04, 0xfe5f4fffffffffff, 0xfec1fff60ffff874, 0xee5e6dffffffffff, 0xffc1fffe0fff87f4, 0xfe5e0fffffffffff, 0xfff8ffffc7f87ffe, 0x7cfe8fffffffffff, 0xffc007e00007fffe, 0x38f003ffffffffff, 0xffb8181dc0fffffe, 0xe007ffffffffff, 0xffa003fd001ffffe, 0xc01fffffffffff, 0xffdc03fee01fffde, 0x807fffffffffff, 0xffbfc3fdfe1fffb6, 0x3800ffffffffffff, 0xffbfc7fdfe3fff67, 0xfe00ffffffffffff, 0xffdfe7feff3fff70, 0xffffffffffff, 0xfffff3ffff9fff78, 0x7fffffffffff, 0xfffff9ffffcfffbc, 0x7fffffffffff, 0xfffffcffffe7ffdf, 0xfff7ffffffffffff, 0xffffffffffffffe0, 0x1fffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};



uint64_t red_bin_ny_out[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffefffff7fffff07, 0xffdfffffffffffff, 0xfffffffffffff877, 0xefde6fffffffffff, 0xffffffffffff87f7, 0xffde4fffffffffff, 0xfffffffffff87fff, 0xfffecfffffffffff, 0xffff87e1fc07ffff, 0xfff003ffffffffff, 0xffff781ffbfffffe, 0x38e007ffffffffff, 0xffff7ffffbfffffe, 0x38c01fffffffffff, 0xfffefffff7ffffff, 0xff807fffffffffff, 0xfffffffffffffff7, 0xff00ffffffffffff, 0xffffffffffffffe7, 0xfe00ffffffffffff, 0xfffffffffffffff0, 0xffffffffffff, 0xfffffffffffffff8, 0x7fffffffffff, 0xfffffffffffffffc, 0x7fffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};
uint64_t green_bin_ny_out[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xe1ffffffffffffff, 0xfffdffffefffffff, 0xc3ffffffffffffff, 0xfff97fffcbffffff, 0x83ffffffffffffff, 0xfffa7fffd3ffffff, 0x1ffffffffffffff, 0xfff9ffffcfffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xfffffbffffffffff, 0xffffffffffffffff, 0xeffffeffffffffff, 0xffefffff7fffff04, 0xfe5fefffffffffff, 0xfefffff7fffff874, 0xee5ffdffffffffff, 0xffffffffffff87f4, 0xfe5fbfffffffffff, 0xfffffffffff87ffe, 0x7cffffffffffffff, 0xffff87e1fc07fffe, 0x38f003ffffffffff, 0xffff781ffbfffffe, 0x38e007ffffffffff, 0xffbf7ffdfbfffffe, 0xc01fffffffffff, 0xfffefffff7fffffe, 0x807fffffffffff, 0xfffffffffffffff6, 0x3800ffffffffffff, 0xffffffffffffffe7, 0xfe00ffffffffffff, 0xffdffffefffffff0, 0xffffffffffff, 0xfffffffffffffff8, 0x7fffffffffff, 0xfffffffffffffffc, 0x7fffffffffff, 0xfffffcffffe7ffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};
uint64_t blue_bin_ny_out[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xe1ffffffffffffff, 0xfffdffffefffffff, 0xc3ffffffffffffff, 0xfff97fffcbffffff, 0x83ffffffffffffff, 0xfffa7fffd3ffffff, 0x1ffffffffffffff, 0xffe9ffff4fffffff, 0xffffffffffffffff, 0xffe1ffff0fffffff, 0xfffffbffffffffff, 0xffcbfffe5fffffff, 0xfffffeffffffffff, 0xff88fffc47ffff04, 0xfe5f4fffffffffff, 0xfec1fff60ffff874, 0xee5e6dffffffffff, 0xffc1fffe0fff87f4, 0xfe5e0fffffffffff, 0xfff8ffffc7f87ffe, 0x7cfe8fffffffffff, 0xffc007e00007fffe, 0x38f003ffffffffff, 0xffb8181dc0fffffe, 0xe007ffffffffff, 0xffa003fd001ffffe, 0xc01fffffffffff, 0xffdc03fee01fffde, 0x807fffffffffff, 0xffbfc3fdfe1fffb6, 0x3800ffffffffffff, 0xffbfc7fdfe3fff67, 0xfe00ffffffffffff, 0xffdfe7feff3fff70, 0xffffffffffff, 0xfffff3ffff9fff78, 0x7fffffffffff, 0xfffff9ffffcfffbc, 0x7fffffffffff, 0xfffffcffffe7ffdf, 0xfff7ffffffffffff, 0xffffffffffffffe0, 0x1fffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};




uint64_t red_bin_out[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x800000008000000, 0x40000000400, 0x1c0000001c000000, 0x40000000400, 0x0, 0xe0000000e00, 0x0, 0x0, 0x200, 0x801040000000c10, 0x40000, 0x810000000000000, 0x400000004000000, 0x60000001800, 0x200000002000400, 0x0, 0x1000000010040100, 0x114a0000001619, 0x200000002000800, 0x818083000004003, 0x50300000504a8000, 0x2221053f00002400, 0x91fe01d09217ff8, 0xe711103ff00f0110, 0x221ffffc22053fff, 0xff01001ffffe0018, 0xffffffffffffffff, 0xffcff9fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};



uint64_t green_bin_out[1][64]={{0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x800000008000000, 0x40000000400, 0x1c0000001c000000, 0x40000000400, 0x800000008000000, 0xe0000000e00, 0x1c0000001c000000, 0x40000000410, 0x1c0000001c000200, 0x8010e0000000e10, 0x2e0000002e040200, 0x8110e0000000e10, 0x3f0000003f040700, 0x8139d0000000718, 0x7d0000007d0e0b00, 0x1c3b9f0000001f3d, 0x6f8000006f8f0f80, 0x182ff780000039bf, 0xff800000ff9f1f80, 0x367fffb000007fff, 0xfff00001fff5bfc0, 0x7fffffff0000ffff, 0xf7ffe01df7dff9f8, 0xe7fefffff00fffef, 0xfffffffffffeffff, 0xffffffffffffffff, 0xe3ffffffe3f3ffff, 0xffcff1fffffff3ff, 0xe3ffffffe3f3ffff, 0xfffff1fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};



uint64_t blue_bin_out[1][64]={{0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xf7fffffff7ffffff, 0xfffffbfffffffbff, 0xe3ffffffe3ffffff, 0xfffffbfffffffbff, 0xf7fffffff7ffffff, 0xfffff1fffffff1ff, 0xe3ffffffe3ffffff, 0xfffffbffffffffff, 0xe3ffffffe3fffdff, 0xf7fef1fffffff3ff, 0xd1ffffffd1fbfdff, 0xf7eef1fffffff1ff, 0xc4ffffffc4fbf8ff, 0xf7ec60ffffffe1e7, 0x80ffffff80f1f0ff, 0xe3c460ffffffe1c6, 0x807fffff8070f17f, 0xe7c1427fffffc040, 0x27fffff0260e87f, 0xc198007fffffc001, 0x103ffffe1040c03f, 0xa220053fffffa000, 0x11ffffe010079ff, 0xe730103fffff0100, 0x229ffffc22843fff, 0xff01001ffffe0018, 0xe3ffffffe3f3ffff, 0xffcff1fffffff3ff, 0xe3ffffffe3f3ffff, 0xfffff1fffffff3ff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff, },
};








int portA = 0;





int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  int capture = 0;
  char buf[25];

//  int Y = 10;
  int X = 0;
  int PANEL_X_MAX=128;


//  memcpy(command.red_bin_u, red_bin, sizeof(command.red_bin_u));
//  memcpy(command.green_bin_u, green_bin, sizeof(command.green_bin_u));
//  memcpy(command.blue_bin_u, blue_bin, sizeof(command.blue_bin_u));

//  int time = 0;
  int last_time = HAL_GetTick();

  while (1)
  {
	  if(HAL_GetTick() - last_time > 100){
		  X++;
		  last_time = HAL_GetTick();
	  }
	  if(X >=128){
		  X = 0;
	  }



//	  capture = TIM1->CNT;
//	  X = round(capture/100);
//	  sprintf(buf,"count: [%ld]\n", sizeof(red_bin));
//	  HAL_UART_Transmit_DMA(&huart3,buf,sizeof(buf)-1);

//	  HAL_UART_Receive_DMA(&huart3, (int *)&command, sizeof(red_bin)*3-1);
//
//
//	  memcpy(red_bin_out, red_bin, sizeof(red_bin));
//	  memcpy(green_bin_out, green_bin, sizeof(green_bin));
//	  memcpy(blue_bin_out, blue_bin, sizeof(blue_bin));

	  memcpy(blue_bin_ny_out, blue_bin_ny, sizeof(blue_bin_ny));
	  memcpy(red_bin_ny_out, blue_bin_ny, sizeof(blue_bin_ny));
	  memcpy(green_bin_ny_out, green_bin_ny, sizeof(green_bin_ny));

	  memcpy(blue_bin_ny__out, blue_bin_ny__, sizeof(blue_bin_ny__));
	  memcpy(red_bin_ny__out, blue_bin_ny__, sizeof(blue_bin_ny__));
	  memcpy(green_bin_ny__out, green_bin_ny__, sizeof(green_bin_ny__));

	  for(int xx = 0;xx < 32; xx++){


		  if(X <64){
			  int sd = 63;
			  blue_bin_ny_out[0][xx*2] = (blue_bin_ny[0][xx*2] << X) | (blue_bin_ny[0][xx*2+1] >> (sd-X));
			  blue_bin_ny_out[0][xx*2+1] = (blue_bin_ny[0][xx*2+1] << X) | (blue_bin_ny[0][xx*2] >> (sd-X));
			  green_bin_ny_out[0][xx*2] = (green_bin_ny[0][xx*2] << X) | (green_bin_ny[0][xx*2+1] >> (sd-X));
			  green_bin_ny_out[0][xx*2+1] = (green_bin_ny[0][xx*2+1] << X) | (green_bin_ny[0][xx*2] >> (sd-X));
			  red_bin_ny_out[0][xx*2] = (red_bin_ny[0][xx*2] << X) | (red_bin_ny[0][xx*2+1] >> (sd-X));
			  red_bin_ny_out[0][xx*2+1] = (red_bin_ny[0][xx*2+1] << X) | (red_bin_ny[0][xx*2] >> (sd-X));


			  blue_bin_ny__out[0][xx*2] = (blue_bin_ny__[0][xx*2] << X) | (blue_bin_ny__[0][xx*2+1] >> (sd-X));
			  blue_bin_ny__out[0][xx*2+1] = (blue_bin_ny__[0][xx*2+1] << X) | (blue_bin_ny__[0][xx*2] >> (sd-X));
			  green_bin_ny__out[0][xx*2] = (green_bin_ny__[0][xx*2] << X) | (green_bin_ny__[0][xx*2+1] >> (sd-X));
			  green_bin_ny__out[0][xx*2+1] = (green_bin_ny__[0][xx*2+1] << X) | (green_bin_ny__[0][xx*2] >> (sd-X));
			  red_bin_ny__out[0][xx*2] = (red_bin_ny__[0][xx*2] << X) | (red_bin_ny__[0][xx*2+1] >> (sd-X));
			  red_bin_ny__out[0][xx*2+1] = (red_bin_ny__[0][xx*2+1] << X) | (red_bin_ny__[0][xx*2] >> (sd-X));
		  }
		  if(X >=64){
			  int sd = 127;
			  blue_bin_ny_out[0][xx*2+1] = (blue_bin_ny[0][xx*2] << (X-63)) | (blue_bin_ny[0][xx*2+1] >> (sd-X));
			  blue_bin_ny_out[0][xx*2] = (blue_bin_ny[0][xx*2+1] << (X-63)) | (blue_bin_ny[0][xx*2] >> (sd-X));
			  green_bin_ny_out[0][xx*2+1] = (green_bin_ny[0][xx*2] << (X-63)) | (green_bin_ny[0][xx*2+1] >> (sd-X));
			  green_bin_ny_out[0][xx*2] = (green_bin_ny[0][xx*2+1] << (X-63)) | (green_bin_ny[0][xx*2] >> (sd-X));
			  red_bin_ny_out[0][xx*2+1] = (red_bin_ny[0][xx*2] << (X-63)) | (red_bin_ny[0][xx*2+1] >> (sd-X));
			  red_bin_ny_out[0][xx*2] = (red_bin_ny[0][xx*2+1] << (X-63)) | (red_bin_ny[0][xx*2] >> (sd-X));


			  blue_bin_ny__out[0][xx*2+1] = (blue_bin_ny__[0][xx*2] << (X-63)) | (blue_bin_ny__[0][xx*2+1] >> (sd-X));
			  blue_bin_ny__out[0][xx*2] = (blue_bin_ny__[0][xx*2+1] << (X-63)) | (blue_bin_ny__[0][xx*2] >> (sd-X));
			  green_bin_ny__out[0][xx*2+1] = (green_bin_ny__[0][xx*2] << (X-63)) | (green_bin_ny__[0][xx*2+1] >> (sd-X));
			  green_bin_ny__out[0][xx*2] = (green_bin_ny__[0][xx*2+1] << (X-63)) | (green_bin_ny__[0][xx*2] >> (sd-X));
			  red_bin_ny__out[0][xx*2+1] = (red_bin_ny__[0][xx*2] << (X-63)) | (red_bin_ny__[0][xx*2+1] >> (sd-X));
			  red_bin_ny__out[0][xx*2] = (red_bin_ny__[0][xx*2+1] << (X-63)) | (red_bin_ny__[0][xx*2] >> (sd-X));
		  }
	  }

	  memcpy(red_bin_out, red_bin, sizeof(red_bin));
	  memcpy(green_bin_out, green_bin, sizeof(green_bin));
	  memcpy(blue_bin_out, blue_bin, sizeof(blue_bin));

	  for(int zz = 0;zz < 1; zz++){
		  for(int xx = 0;xx < 64; xx++){
			  blue_bin_out[zz][xx] = (blue_bin_out[zz][xx] & blue_bin_ny_out[zz][xx]) | (blue_bin_ny_out[zz][xx] & blue_bin_ny__out[0][xx]);
			  green_bin_out[zz][xx] = (green_bin_out[zz][xx] & green_bin_ny_out[zz][xx]) | (green_bin_ny_out[zz][xx] & green_bin_ny__out[0][xx]);
			  red_bin_out[zz][xx] = (red_bin_out[zz][xx] & red_bin_ny_out[zz][xx]) | (red_bin_ny_out[zz][xx] & red_bin_ny__out[0][xx]);
		  }
	  }

	  int Y = 0;

	  for(int z = 0; z<1; z++){
	  for( int j = 0; j<16;j++){
//		  HAL_Delay(1);
//		  for(int d=0;d<0x500;d++);
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);

		  Y= j;
		  portA = 0xFFFFFFFF;
		  set_row(Y);
//		  Y++;


//		  } // for X


		  uint64_t one = 1;
		  for (int i = 0; i < 64; i++)
		  {
			  //top
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y*2]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y*2]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y*2]>>(63-i)&one));
//			  //bottom
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y*2+32]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y*2+32]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y*2+32]>>(63-i)&one));

			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin_out[z][Y*2]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin_out[z][Y*2]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin_out[z][Y*2]>>(63-i)&one));
			  //bottom
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin_out[z][Y*2+32]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin_out[z][Y*2+32]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin_out[z][Y*2+32]>>(63-i)&one));
			  clock();
		  } // for X
		  for (int i = 0; i < 64; i++)
		  {
//			  //top
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin[z][Y*2+1]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin[z][Y*2+1]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin[z][Y*2+1]>>(63-i)&one));
//			  //bottom
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin[z][Y*2+32+1]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin[z][Y*2+32+1]>>(63-i)&one));
//			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin[z][Y*2+32+1]>>(63-i)&one));
//			  clock();
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_3,(green_bin_out[z][Y*2+1]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,(red_bin_out[z][Y*2+1]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,(blue_bin_out[z][Y*2+1]>>(63-i)&one));
			  //bottom
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,(green_bin_out[z][Y*2+32+1]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,(red_bin_out[z][Y*2+32+1]>>(63-i)&one));
			  HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,(blue_bin_out[z][Y*2+32+1]>>(63-i)&one));
			  clock();
		  } // for X
		  lat();
		  oe();
//		  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  } // for Y
	  }//z


	} //while

  } //main

void clock(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_4,GPIO_PIN_SET);
//	  portA = portA & 0b11111111111111111111111111101111;
//	  GPIOA->ODR = portA;
//	  portA = portA | 0b00000000000000000000000000010000;
//	  GPIOA->ODR = portA;
}

void lat(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_SET);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);

//	  portA = portA & 0b11111111111111111111111110111111;
//	  GPIOA->ODR = portA;
//	  portA = portA | 0b00000000000000000000000001000000;
//	  GPIOA->ODR = portA;
//	  portA = portA & 0b11111111111111111111111110111111;
//	  GPIOA->ODR = portA;
}
void oe(){
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_RESET);
	  for(int d=0;d<0x10;d++);
	  HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);

//	  portA = portA & 0b11111111111111111111111111011111;
//	  GPIOA->ODR = portA;
//	  for(int d=0;d<0x5;d++);
//	  portA = portA | 0b00000000000000000000000000100000;
//	  GPIOA->ODR = portA;
}

void set_row(int row){
	  if (row & 0x01) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_0,GPIO_PIN_RESET);

	  if (row & 0x02) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_1,GPIO_PIN_RESET);

	  if (row & 0x04) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_2,GPIO_PIN_RESET);

	  if (row & 0x08) HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_SET);
	  else HAL_GPIO_WritePin(GPIOA,GPIO_PIN_3,GPIO_PIN_RESET);
//	row = row | 0b11111111111111111111111111110000;
//	portA = row | 0b11111111111111111111111111110000;
//	GPIOA->ODR = portA;
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 12800;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 57600;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3
                           PA4 PA5 PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB3 PB4 PB5 PB6
                           PB7 PB8 */
  GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6
                          |GPIO_PIN_7|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
